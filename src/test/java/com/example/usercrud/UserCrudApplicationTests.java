package com.example.usercrud;

import com.example.usercrud.exception.UserNotFound;
import com.example.usercrud.mapper.UserMapStructMapper;
import com.example.usercrud.model.UserDto;
import com.example.usercrud.model.UserEntity;
import com.example.usercrud.repository.UserRepository;
import com.example.usercrud.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserCrudApplicationTests {
    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapStructMapper userMapStructMapper;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void testGetUserById() {
        Long userId = 1L;
        UserEntity mockUserEntity = new UserEntity();
        UserDto mockUserDto = new UserDto();
        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUserEntity));
        when(userMapStructMapper.userToDto(mockUserEntity)).thenReturn(mockUserDto);

        UserDto result = userService.getUserById(userId);

        assertEquals(mockUserDto, result);
    }

    @Test
    public void testGetAllUsers() {
        List<UserEntity> mockUserEntities = Arrays.asList(new UserEntity(), new UserEntity());
        List<UserDto> mockUserDtos = Arrays.asList(new UserDto(), new UserDto());
        when(userRepository.findAll()).thenReturn(mockUserEntities);
        when(userMapStructMapper.userToDto(Mockito.any(UserEntity.class)))
                .thenReturn(mockUserDtos.get(0), mockUserDtos.get(1));

        List<UserDto> result = userService.getAllUsers();

        assertEquals(mockUserDtos, result);
    }
    @Test
    public void testAddUser() {
        UserDto mockUserDto = new UserDto();
        UserEntity mockUserEntity = new UserEntity();
        when(userMapStructMapper.userToEntity(mockUserDto)).thenReturn(mockUserEntity);
        when(userRepository.save(mockUserEntity)).thenReturn(mockUserEntity);
        when(userMapStructMapper.userToDto(mockUserEntity)).thenReturn(mockUserDto);

        UserDto result = userService.addUser(mockUserDto);

        assertEquals(mockUserDto, result);
    }

    @Test
    public void testUpdateUser() {
        Long userId = 1L;
        UserDto mockUserDto = new UserDto();
        UserEntity mockUserEntity = new UserEntity();
        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUserEntity));
        when(userRepository.save(mockUserEntity)).thenReturn(mockUserEntity);
        when(userMapStructMapper.userToEntity(mockUserDto)).thenReturn(mockUserEntity);
        when(userMapStructMapper.userToDto(mockUserEntity)).thenReturn(mockUserDto);

        UserDto result = userService.updateUser(mockUserDto, userId);

        assertEquals(mockUserDto, result);
    }

    @Test
    public void testDeleteUserById() {
        Long userId = 1L;
        UserEntity mockUserEntity = new UserEntity();
        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUserEntity));

        userService.deleteUserById(userId);

        Mockito.verify(userRepository, Mockito.times(1)).deleteById(userId);
    }

    @Test
    public void testCheckUserPresence_UserNotFound() {
        Long userId = 1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(UserNotFound.class, () -> userService.checkUserPresence(userId));
    }

}
