FROM openjdk:17-jdk-slim-buster

WORKDIR /app

COPY build/libs/user-crud-0.0.1-SNAPSHOT.jar app.jar

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /app/wait-for-it.sh
RUN chmod +x /app/wait-for-it.sh

EXPOSE 8585

CMD ["/app/wait-for-it.sh", "mysql:3306", "--", "java", "-jar", "app.jar"]